

    Day 1 - Genesis 1-4

    Day 2 - Genesis 5-8

    Day 3 - Genesis 9-12

    Day 4 - Genesis 13-17

    Day 5 - Genesis 18-20

    Day 6 - Genesis 21-23

    Day 7 - Genesis 24-25

    Day 8 - Genesis 26-28

    Day 9 - Genesis 29-31

    Day 10 - Genesis 32-35

    Day 11 - Genesis 36-38

    Day 12 - Genesis 39-41

    Day 13 - Genesis 42-43

    Day 14 - Genesis 44-46

    Day 15 - Genesis 47-50

    Day 16 - Exodus 1-4

    Day 17 - Exodus 5-7

    Day 18 - Exodus 8-10

    Day 19 - Exodus 11-13

    Day 20 - Exodus 14-16

    Day 21 - Exodus 17-20

    Day 22 - Exodus 21-23

    Day 23 - Exodus 24-27

    Day 24 - Exodus 28-30

    Day 25 - Exodus 31-34

    Day 26 - Exodus 35-37

    Day 27 - Exodus 38-40

    Day 28 - Leviticus 1-4

    Day 29 - Leviticus 5-7

    Day 30 - Leviticus 8-10

    Day 31 - Leviticus 11-13

    Day 32 - Leviticus 14-15

    Day 33 - Leviticus 16-18

    Day 34 - Leviticus 19-21

    Day 35 - Leviticus 22-23

    Day 36 - Leviticus 24-25

    Day 37 - Leviticus 26-27

    Day 38 - Numbers 1-2

    Day 39 - Numbers 3-4

    Day 40 - Numbers 5-6

    Day 41 - Numbers 7

    Day 42 - Numbers 8-10

    Day 43 - Numbers 11-13

    Day 44 - Numbers 14-15

    Day 45 - Numbers 16-18

    Day 46 - Numbers 19-21

    Day 47 - Numbers 22-24

    Day 48 - Numbers 25-26

    Day 49 - Numbers 27-29

    Day 50 - Numbers 30-32

    Day 51 - Numbers 33-36

    Day 52 - Deuteronomy 1-2

    Day 53 - Deuteronomy 3-4

    Day 54 - Deuteronomy 5-8

    Day 55 - Deuteronomy 9-11

    Day 56 - Deuteronomy 12-15

    Day 57 - Deuteronomy 16-19

    Day 58 - Deuteronomy 20-22

    Day 59 - Deuteronomy 23-25

    Day 60 - Deuteronomy 26-27

    Day 61 - Deuteronomy 28-29

    Day 62 - Deuteronomy 30-32

    Day 63 - Deuteronomy 33-34

    Day 64 - Joshua 1-4

    Day 65 - Joshua 5-7

    Day 66 - Joshua 8-10

    Day 67 - Joshua 11-13

    Day 68 - Joshua 14-17

    Day 69 - Joshua 18-20

    Day 70 - Joshua 21-22

    Day 71 - Joshua 23-24

    Day 72 - Judges 1-3

    Day 73 - Judges 4-5

    Day 74 - Judges 6-8

    Day 75 - Judges 9-10

    Day 76 - Judges 11-13

    Day 77 - Judges 14-16

    Day 78 - Judges 17-19

    Day 79 - Judges 20-21

    Day 80 - Ruth 1-4

    Day 81 - 1 Samuel 1-3

    Day 82 - 1 Samuel 4-7

    Day 83 - 1 Samuel 8-12

    Day 84 - 1 Samuel 13-14

    Day 85 - 1 Samuel 15-16

    Day 86 - 1 Samuel 17-18

    Day 87 - 1 Samuel 19-21

    Day 88 - 1 Samuel 22-24

    Day 89 - 1 Samuel 25-27

    Day 90 - 1 Samuel 28-31

    Day 91 - 2 Samuel 1-3

    Day 92 - 2 Samuel 4-7

    Day 93 - 2 Samuel 8-11

    Day 94 - 2 Samuel 12-13

    Day 95 - 2 Samuel 14-16

    Day 96 - 2 Samuel 17-19

    Day 97 - 2 Samuel 20-22

    Day 98 - 2 Samuel 23-24

    Day 99 - 1 Kings 1-2

    Day 100 - 1 Kings 3-5

    Day 101 - 1 Kings 6-7

    Day 102 - 1 Kings 8-9

    Day 103 - 1 Kings 10-12

    Day 104 - 1 Kings 13-15

    Day 105 - 1 Kings 16-18

    Day 106 - 1 Kings 19-20

    Day 107 - 1 Kings 21-22

    Day 108 - 2 Kings 1-3

    Day 109 - 2 Kings 4-5

    Day 110 - 2 Kings 6-8

    Day 111 - 2 Kings 9-10

    Day 112 - 2 Kings 11-13

    Day 113 - 2 Kings 14-16

    Day 114 - 2 Kings 17-18

    Day 115 - 2 Kings 19-21

    Day 116 - 2 Kings 22-23

    Day 117 - 2 Kings 24-25

    Day 118 - 1 Chronicles 1-2

    Day 119 - 1 Chronicles 3-4

    Day 120 - 1 Chronicles 5-6

    Day 121 - 1 Chronicles 7-9

    Day 122 - 1 Chronicles 10-12

    Day 123 - 1 Chronicles 13-16

    Day 124 - 1 Chronicles 17-19

    Day 125 - 1 Chronicles 20-23

    Day 126 - 1 Chronicles 24-26

    Day 127 - 1 Chronicles 27-29

    Day 128 - 2 Chronicles 1-4

    Day 129 - 2 Chronicles 5-7

    Day 130 - 2 Chronicles 8-11

    Day 131 - 2 Chronicles 12-16

    Day 132 - 2 Chronicles 17-20

    Day 133 - 2 Chronicles 21-24

    Day 134 - 2 Chronicles 25-28

    Day 135 - 2 Chronicles 29-31

    Day 136 - 2 Chronicles 32-34

    Day 137 - 2 Chronicles 35-36

    Day 138 - Ezra 1-4

    Day 139 - Ezra 5-7

    Day 140 - Ezra 8-10

    Day 141 - Nehemiah 1-3

    Day 142 - Nehemiah 4-7

    Day 143 - Nehemiah 8-10

    Day 144 - Nehemiah 11-13

    Day 145 - Esther 1-5

    Day 146 - Esther 6-10

    Day 147 - Job 1-4

    Day 148 - Job 5-8

    Day 149 - Job 9-12

    Day 150 - Job 13-16

    Day 151 - Job 17-20

    Day 152 - Job 21-24

    Day 153 - Job 25-30

    Day 154 - Job 31-34

    Day 155 - Job 35-38

    Day 156 - Job 39-42

    Day 157 - Psalms 1-8

    Day 158 - Psalms 9-17

    Day 159 - Psalms 18-21

    Day 160 - Psalms 22-27

    Day 161 - Psalms 28-33

    Day 162 - Psalms 34-37

    Day 163 - Psalms 38-42

    Day 164 - Psalms 43-49

    Day 165 - Psalms 50-55

    Day 166 - Psalms 56-61

    Day 167 - Psalms 62-68

    Day 168 - Psalms 69-72

    Day 169 - Psalms 73-77

    Day 170 - Psalms 78-80

    Day 171 - Psalms 81-88

    Day 172 - Psalms 89-94

    Day 173 - Psalms 95-103

    Day 174 - Psalms 104-106

    Day 175 - Psalms 107-111

    Day 176 - Psalms 112-118

    Day 177 - Psalm 119

    Day 178 - Psalms 120-133

    Day 179 - Psalms 134-140

    Day 180 - Psalms 141-150

    Day 181 - Proverbs 1-3

    Day 182 - Proverbs 4-7

    Day 183 - Proverbs 8-11

    Day 184 - Proverbs 12-14

    Day 185 - Proverbs 15-17

    Day 186 - Proverbs 18-20

    Day 187 - Proverbs 21-23

    Day 188 - Proverbs 24-26

    Day 189 - Proverbs 27-29

    Day 190 - Proverbs 30-31

    Day 191 - Ecclesiastes 1-4

    Day 192 - Ecclesiastes 5-8

    Day 193 - Ecclesiastes 9-12

    Day 194 - Song of Solomon 1-4

    Day 195 - Song of Solomon 5-8

    Day 196 - Isaiah 1-3

    Day 197 - Isaiah 4-8

    Day 198 - Isaiah 9-11

    Day 199 - Isaiah 12-14

    Day 200 - Isaiah 15-19

    Day 201 - Isaiah 20-24

    Day 202 - Isaiah 25-28

    Day 203 - Isaiah 29-31

    Day 204 - Isaiah 32-34

    Day 205 - Isaiah 35-37

    Day 206 - Isaiah 38-40

    Day 207 - Isaiah 41-43

    Day 208 - Isaiah 44-46

    Day 209 - Isaiah 47-49

    Day 210 - Isaiah 50-52

    Day 211 - Isaiah 53-56

    Day 212 - Isaiah 57-59

    Day 213 - Isaiah 60-63

    Day 214 - Isaiah 64-66

    Day 215 - Jeremiah 1-3

    Day 216 - Jeremiah 4-5

    Day 217 - Jeremiah 6-8

    Day 218 - Jeremiah 9-11

    Day 219 - Jeremiah 12-14

    Day 220 - Jeremiah 15-17

    Day 221 - Jeremiah 18-21

    Day 222 - Jeremiah 22-24

    Day 223 - Jeremiah 25-27

    Day 224 - Jeremiah 28-30

    Day 225 - Jeremiah 31-32

    Day 226 - Jeremiah 33-36

    Day 227 - Jeremiah 37-39

    Day 228 - Jeremiah 40-43

    Day 229 - Jeremiah 44-46

    Day 230 - Jeremiah 47-48

    Day 231 - Jeremiah 49

    Day 232 - Jeremiah 50

    Day 233 - Jeremiah 51-52

    Day 234 - Lamentations 1-2

    Day 235 - Lamentations 3-5

    Day 236 - Ezekiel 1-4

    Day 237 - Ezekiel 5-8

    Day 238 - Ezekiel 9-12

    Day 239 - Ezekiel 13-15

    Day 240 - Ezekiel 16-17

    Day 241 - Ezekiel 18-20

    Day 242 - Ezekiel 21-22

    Day 243 - Ezekiel 23-24

    Day 244 - Ezekiel 25-27

    Day 245 - Ezekiel 28-30

    Day 246 - Ezekiel 31-32

    Day 247 - Ezekiel 33-35

    Day 248 - Ezekiel 36-38

    Day 249 - Ezekiel 39-40

    Day 250 - Ezekiel 41-43

    Day 251 - Ezekiel 44-46

    Day 252 - Ezekiel 47-48

    Day 253 - Daniel 1-3

    Day 254 - Daniel 4-5

    Day 255 - Daniel 6-8

    Day 256 - Daniel 9-12

    Day 257 - Hosea 1-4

    Day 258 - Hosea 5-9

    Day 259 - Hosea 10-14

    Day 260 - Joel 1-3

    Day 261 - Amos 1-4

    Day 262 - Amos 5-9

    Day 263 - Obadiah 1

    Day 264 - Jonah 1-4

    Day 265 - Micah 1-4

    Day 266 - Micah 5-7

    Day 267 - Nahum 1-3

    Day 268 - Habakkuk 1-3

    Day 269 - Zephaniah 1-3

    Day 270 - Haggai 1-2

    Day 271 - Zechariah 1-5

    Day 272 - Zechariah 6-10

    Day 273 - Zechariah 11-14

    Day 274 - Malachi 1-4

    Day 275 - Matthew 1-4

    Day 276 - Matthew 5-6

    Day 277 - Matthew 7-9

    Day 278 - Matthew 10-11

    Day 279 - Matthew 12-13

    Day 280 - Matthew 14-17

    Day 281 - Matthew 18-20

    Day 282 - Matthew 21-22

    Day 283 - Matthew 23-24

    Day 284 - Matthew 25-26

    Day 285 - Matthew 27-28

    Day 286 - Mark 1-3

    Day 287 - Mark 4-5

    Day 288 - Mark 6-7

    Day 289 - Mark 8-9

    Day 290 - Mark 10-11

    Day 291 - Mark 12-13

    Day 292 - Mark 14

    Day 293 - Mark 15-16

    Day 294 - Luke 1-2

    Day 295 - Luke 3-4

    Day 296 - Luke 5-6

    Day 297 - Luke 7-8

    Day 298 - Luke 9-10

    Day 299 - Luke 11-12

    Day 300 - Luke 13-15

    Day 301 - Luke 16-18

    Day 302 - Luke 19-20

    Day 303 - Luke 21-22

    Day 304 - Luke 23-24

    Day 305 - John 1-2

    Day 306 - John 3-4

    Day 307 - John 5-6

    Day 308 - John 7-8

    Day 309 - John 9-10

    Day 310 - John 11-12

    Day 311 - John 13-15

    Day 312 - John 16-17

    Day 313 - John 18-19

    Day 314 - John 20-21

    Day 315 - Acts 1-3

    Day 316 - Acts 4-5

    Day 317 - Acts 6-7

    Day 318 - Acts 8-9

    Day 319 - Acts 10-11

    Day 320 - Acts 12-13

    Day 321 - Acts 14-15

    Day 322 - Acts 16-17

    Day 323 - Acts 18-19

    Day 324 - Acts 20-21

    Day 325 - Acts 22-23

    Day 326 - Acts 24-26

    Day 327 - Acts 27-28

    Day 328 - Romans 1-3

    Day 329 - Romans 4-7

    Day 330 - Romans 8-10

    Day 331 - Romans 11-14

    Day 332 - Romans 15-16

    Day 333 - 1 Corinthians 1-4

    Day 334 - 1 Corinthians 5-9

    Day 335 - 1 Corinthians 10-13

    Day 336 - 1 Corinthians 14-16

    Day 337 - 2 Corinthians 1-4

    Day 338 - 2 Corinthians 5-9

    Day 339 - 2 Corinthians 10-13

    Day 340 - Galatians 1-3

    Day 341 - Galatians 4-6

    Day 342 - Ephesians 1-3

    Day 343 - Ephesians 4-6

    Day 344 - Philippians 1-4

    Day 345 - Colossians 1-4

    Day 346 - 1 Thessalonians 1-5

    Day 347 - 2 Thessalonians 1-3

    Day 348 - 1 Timothy 1-6

    Day 349 - 2 Timothy 1-4

    Day 350 - Philemon 1; Titus 1-3

    Day 351 - Hebrews 1-4

    Day 352 - Hebrews 5-8

    Day 353 - Hebrews 9-10

    Day 354 - Hebrews 11-13

    Day 355 - James 1-5

    Day 356 - 1 Peter 1-5; 2 Peter 1-3

    Day 357 - 1 John 1-5

    Day 358 - 2 John 1; 3 John 1; Jude 1

    Day 359 - Revelation 1-3

    Day 360 - Revelation 4-7

    Day 361 - Revelation 8-11

    Day 362 - Revelation 12-14

    Day 363 - Revelation 15-17

    Day 364 - Revelation 18-19

    Day 365 - Revelation 20-22


