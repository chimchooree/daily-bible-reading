import glob
import time
from datetime import date
#import sys
#import requests

API_KEY = '{{ my_key }}'
API_URL = 'https://api.esv.org/v3/passage/text/'

# Get Text at Reference

#def get_esv_text(passage):
#    params = {
#        'q': passage,
#        'include-headings': False,
#        'include-verse-numbers': False,
#        'include-short-copyright': False,
#        'include-passage-references': False
#    }
#    headers = {
#        'Authorization': 'Token %s' % API_KEY
#    }
    
#    response = requests.get(API_URL, params=params, headers=headers)

#    passages = response.json()['passages']

#    return passages[0].strip() if passages else 'Error: Passage not found'


#if __name__ == '__main__':
#    passage = ' '.join(sys.argv[1:])

#    if passage:
#        print(get_esv_text(passage))

# Choose File, Get Today's Reading Reference

def day_code():
    today = date.today()
    return today.strftime("%m%d")

def read_file(list_file):
    daily = list()
    with open(list_file, "r") as f:
        for line in f.readlines():
            if day_code() in line:
                line = line.replace('\n', '')
                daily = line.split("/")
                refs = daily[1].split('\\')
    return refs

# Format directory listing for choosing file
def list_choices(plans):
    choice = "Press "
    for i in range(len(plans)):
        title = plans[i].replace('daily-reading-lists/','')
        title = title.replace('.txt','')
        choice += str(i) + " for " + title
        if i < len(plans) - 1:
            choice += ", "
        if i == len(plans) - 2:
            choice += "or "
    choice += ": "
    return choice

# Main Program Loop
def main():
    plans = glob.glob("daily-reading-lists/*.txt")
    today = date.today()
    today_string = today.strftime("%B %d, %Y")

    # Greeting
    print("Today is " + today_string + ".")

    # Choose Plan
    val = int(raw_input(list_choices(plans)))
    plan = plans[val]
    verses = read_file(plan)
    print(verses)
    #get_esv_text(verses)

## Start Program ##
main()
