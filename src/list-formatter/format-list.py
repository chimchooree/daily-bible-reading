import calendar

test = 'test.txt'

## File with unformatted article
folder = 'daily-reading-lists' 

def format_date(line):
# January 1 -> 0101
    int(s) for s in line.split() if s.isdigit()
    new_line = line.replace('january', %m)    
    new_line = new_line.replace('jan', '01')
    new_line = new_line.replace('day 1', '01')
    return new_line

def replace(line):
    new_line = line.replace('   ', '')
    new_line = new_line.replace('1st', '1')
    new_line = new_line.replace('2nd', '2')
    new_line = new_line.replace('3rd', '3')
    for i in range(10):
        new_line = new_line.replace(str(i) + 'th', str(i))
    new_line = new_line.replace('readings', '')
    return new_line

def format():
    output = ""
    with open(test, "r") as f:
        for line in f.readlines():
            if line != '\n':
                new_line = line.lower()
                new_line = replace(new_line)
                new_line = format_date(new_line)
                output += new_line
        print(output)

format()
