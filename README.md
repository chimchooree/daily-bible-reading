# Requirements
* Python 2.7+
* Key from https://api.esv.org/

# Copyright

    Scripture quotations are from the ESV® Bible (The Holy Bible, English Standard Version®), copyright © 2001 by Crossway, a publishing ministry of Good News Publishers. Used by permission. All rights reserved. You may not copy or download more than 500 consecutive verses of the ESV Bible or more than one half of any book of the ESV Bible.

(https://api.esv.org/#copyright)
